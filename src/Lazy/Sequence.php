<?php

namespace Lazy;


/**
 * Collection of utility methods that operate on iterators in a lazy way using
 * generators
 */
final class Sequence {
    /**
     * Takes any traversable object and converts it into a full-blown iterator
     * 
     * @param array|Traversable $seq
     * @return \Iterator
     */
    protected static function toIterator($seq) {
        // If we have an array, return an array iterator based on it
        if( is_array($seq) ) return new \ArrayIterator($seq);
        
        // If we already have an iterator, return it
        if( $seq instanceof \Iterator ) return $seq;
        
        // If it implements IteratorAggregate, return the iterator
        if( $seq instanceof \IteratorAggregate ) return $seq->getIterator();
        
        // If we have anything else that implements Traversable (no idea what
        // that might be!), convert it to a generator (\Generator implements \Iterator)
        if( $seq instanceof \Traversable ) return static::makeGenerator($seq);
        
        // If we get to here, we have a non-traversable object
        throw new \InvalidArgumentException('$seq must be an array or implement \Traversable');
    }
    
    protected static function makeGenerator(\Traversable $traversable) {
        foreach( $traversable as $key => $value ) {
            yield $key => $value;
        }
    }
    
    /**
     * Get the first element of a sequence
     * 
     * @param array|\Traversable $seq
     * @return mixed
     */
    public static function head($seq) {
        // Convert to an iterator
        $iter = static::toIterator($seq);

        // Make sure we get the first element
        $iter->rewind();
        return $iter->valid() ? $iter->current() : null;
    }

    /**
     * Get the rest of the sequence from the second element onwards
     * 
     * @param array|\Traversable $seq
     * @return \Iterator
     */
    public static function tail($seq) {
        return static::skip($seq, 1);
    }

    /**
     * Get the last element of the sequence
     * 
     * NOTE that the sequence must NOT be infinite, else this enters an infinite loop
     * 
     * @param array|\Traversable $seq
     * @return mixed
     */
    public static function last($seq) {
        return static::head(static::reverse($seq));
    }

    /**
     * Checks if a sequence is empty
     * 
     * @param array|\Traversable $seq
     * @return boolean
     */
    public static function isEmpty($seq) {
        // Convert to an iterator
        $iter = static::toIterator($seq);

        // Make sure we get the first element
        $iter->rewind();
        return !$iter->valid();
    }

    /**
     * Returns a new sequence consisting of $item prepended to $seq
     * 
     * @param array|\Traversable $seq
     * @param mixed $item
     * @return \Iterator
     */
    public static function cons($seq, $item) {
        yield $item;
        
        foreach( $seq as $item ) {
            yield $item;
        }
    }

    /**
     * Returns a new sequence containing all the elements of $seq followed by
     * all the elements of $other
     * 
     * @param array|\Traversable $seq
     * @param array|\Traversable $other
     * @return \Iterator
     */
    public static function append($seq, $other) {
        foreach( $seq as $item ) {
            yield $item;
        }
        
        foreach( $other as $item ) {
            yield $item;
        }
    }

    /**
     * Returns a new sequence consisting of $mapper applied to the elements of $seq
     * 
     * @param array|\Traversable $seq
     * @param callable $mapper
     * @return \Iterator
     */
    public static function map($seq, callable $mapper) {
        foreach( $seq as $item ) {
            yield call_user_func($mapper, $item);
        }
    }

    /**
     * Returns a new sequence consisting of the elements of $seq for which $predicate
     * returns true
     * 
     * @param array|\Traversable $seq
     * @param callable $predicate
     * @return \Iterator
     */
    public static function filter($seq, callable $predicate) {
        foreach( $seq as $item ) {
            if( call_user_func($predicate, $item) ) {
                yield $item;
            }
        }
    }

    /**
     * Returns a new sequence consisting of the first $n items of $seq
     * 
     * @param array|\Traversable $seq
     * @param integer $n
     * @return \Iterator
     */
    public static function take($seq, $n) {
        $i = 0;
        return static::takeWhile($seq, function() use($n, &$i){
            $i++;
            return $i <= $n;
        });
    }

    /**
     * Returns a new sequence consisting of the elements of $seq until the first
     * time that $predicate returns false
     * 
     * @param array|\Traversable $seq
     * @param callable $predicate
     * @return \Iterator
     */
    public static function takeWhile($seq, callable $predicate) {
        foreach( $seq as $item ) {
            if( !call_user_func($predicate, $item) ) {
                break;
            }
            
            yield $item;
        }
    }

    /**
     * Returns a new sequence consisting of all but the first $n elements of $seq
     * 
     * @param array|\Traversable $seq
     * @param integer $n
     * @return \Iterator
     */
    public static function skip($seq, $n) {
        $i = 0;
        return static::skipWhile($seq, function() use($n, &$i){
            $i++;
            return $i <= $n;
        });
    }

    /**
     * Returns a new sequence consisting of all the elements of $seq after the
     * first time that $predicate returns false
     * 
     * @param array|\Traversable $seq
     * @param callable $predicate
     * @return \Iterator
     */
    public static function skipWhile($seq, callable $predicate) {
        $yield = false;
        
        foreach( $seq as $item ) {
            $yield = $yield || !call_user_func($predicate, $item);
            
            if( $yield ) yield $item;
        }
    }

    /**
     * Combines successive elements of $seq using $reducer, with initial value
     * given by $initial
     * 
     * @param array|\Traversable $seq
     * @param callable $reducer
     * @param mixed $initial
     * @return mixed
     */
    public static function reduce($seq, callable $reducer, $initial) {
        $accum = $initial;
        
        foreach( $seq as $item ) {
            $accum = call_user_func($reducer, $accum, $item);
        }
        
        return $accum;
    }

    /**
     * Returns a new sequence that is the reverse of the given sequence
     * 
     * NOTE that calling this with an inifinite sequence will result in an
     * infinite loop
     * 
     * @param array|\Traversable $seq
     * @return \Iterator
     */
    public static function reverse($seq) {
        return static::reduce(
            $seq,
            function($accum, $item) { return static::cons($accum, $item); },
            new \ArrayIterator([])
        );
    }

    /**
     * Returns a new sequence whose values are the associated values from $seq and $other
     * combined using $op
     * 
     * @param array|\Traversable $seq
     * @param array|\Traversable $other
     * @param callable $op
     * @return \Iterator
     */
    public static function zip($seq, $other, callable $op) {
        // Convert the traversables to full-blown iterators
        $iter = static::toIterator($seq);
        $otherIter = static::toIterator($other);
        
        // We need to iterate the sequences together
        $iter->rewind();
        $otherIter->rewind();
        
        while( $iter->valid() && $otherIter->valid() ) {
            yield call_user_func($op, $iter->current(), $otherIter->current());
            
            $iter->next();
            $otherIter->next();
        }
    }

    /**
     * Applies the given function for each element in $seq
     * 
     * @param array|\Traversable $seq
     * @param callable $block
     */
    public static function each($seq, callable $block) {
        foreach( $seq as $item ) {
            call_user_func($block, $item);
        }
    }

    /**
     * Converts a sequence to a regular array
     * 
     * NOTE that if this is called with an infinite sequence, it will result in
     * an infinite loop
     * 
     * @param array|\Traversable $seq
     * @return array
     */
    public static function toArray($seq) {
        $arr = [];
        
        foreach( $seq as $item ) {
            $arr[] = $item;
        }
        
        return $arr;
    }

    /**
     * Returns the number of elements in $seq
     * 
     * NOTE that if $seq is an infinite sequence, this will result in an infinite loop
     * 
     * @param array|\Traversable $seq
     * @return integer
     */
    public static function count($seq) {
        return static::reduce($seq, function($accum, $item) { return $accum + 1; }, 0);
    }

    /**
     * Create a new sequence from the given arguments
     * 
     * @return \Iterator
     */
    public static function create() {
        return static::toIterator(func_get_args());
    }

    /**
     * Create a new sequence consisting of the values from $from to $to (if given)
     * incrementing by $step each time
     * 
     * @param integer $from
     * @param integer|null $to
     * @param integer $step
     * @return \Iterator
     */
    public static function range($from, $to = null, $step = 1) {
        while( true ) {            
            // Break the loop when we get to $to (depending whether we are stepping
            // upwards or downwards to get there)
            if( $to !== null && $step >= 0 && $from > $to ) return;
            if( $to !== null && $step < 0 && $from < $to ) return;
            
            // Yield the current value
            yield $from;
            
            // Increment by the given step
            $from += $step;
        }
    }

    /**
     * Create a new sequence consisting of $x repeated forever
     * 
     * @param mixed $x
     * @return \Iterator
     */
    public static function repeat($x) {
        for(;;) {
            yield $x;
        }
    }

    /**
     * Create a new sequence consisting of $x repeated $n times
     * 
     * @param integer $n
     * @param mixed $x
     * @return \Iterator
     */
    public static function replicate($n, $x) {
        return static::take(static::repeat($x), $n);
    }

    /**
     * Create a new sequence whose values are the result of successively applying
     * $block, with initial value $initial
     * 
     * @param callable $block
     * @param mixed $initial
     * @return \Iterator
     */
    public static function iterate(callable $block, $initial) {
        $current = $initial;
        
        for(;;) {
            yield $current;
            
            $current = call_user_func($block, $current);
        }
    }
}